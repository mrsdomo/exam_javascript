function addEvent(ev, fct) {
    if (window.addEventListener) {
        window.addEventListener(ev, fct, false);
    } else {
        window.attachEvent(ev, fct);
    }
}

addEvent('load', init)
if (window.addEventListener) {
    window.addEventListener('load', init, false);
} else {
    window.attachEvent('onload', init);
}

function init() {
    document.getElementById('menu').setAttribute('onclick', 'active(event,this)')
}

function active(ev, elt) {
    menus = elt.children;
    if(ev.target!=elt){
        for (i = 0; i < 3; i++) {
            menus[i].removeAttribute('class')
        }
        ev.target.setAttribute('class', 'actif')
    }
}